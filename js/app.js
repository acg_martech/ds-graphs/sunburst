(function(d3) {
    'use strict';

    var data = [{
        name: 'Domingo / 2 p.m / Lima / Smart Phone / DoubleClick  / 300x250',
        amount: 600,
        percentage: 0
    }, {
        name: 'Domingo / 6 p.m / Lima / Smart Phone / DoubleClick  / 300x600',
        amount: 600,
        percentage: 0
    }, {
        name: 'Jueves / 12 p.m / Huanuco / Desktop / DoubleClick  / 300x250',
        amount: 600,
        percentage: 100
    }, {
        name: 'Jueves / 12 p.m / Lima / Desktop / BidSwitch / 728x90',
        amount: 600,
        percentage: 100
    }, {
        name: 'Jueves / 5 p.m / Arequipa / Smart Phone / DoubleClick  / 300x600',
        amount: 600,
        percentage: 100
    }, {
        name: 'Lunes / 2 p.m / Lima / Smart Phone / DoubleClick  / 300x600',
        amount: 600,
        percentage: 100
    }, {
        name: 'Lunes / 4 p.m / Lima / Smart Phone / DoubleClick  / 300x250',
        amount: 600,
        percentage: 100
    }, {
        name: 'Martes / 10 p.m / Lima / Smart Phone / DoubleClick  / 300x250',
        amount: 600,
        percentage: 78
    }, {
        name: 'Martes / 6 p.m / Jaen / Smart Phone / DoubleClick  / 300x250',
        amount: 600,
        percentage: 76
    }, {
        name: 'Viernes / 2 a.m / Lima / Smart Phone / DoubleClick  / 300x600',
        amount: 600,
        percentage: 65
    }, {
        name: 'Viernes / 8 p.m / Arequipa / Smart Phone / DoubleClick  / 300x250',
        amount: 600,
        percentage: 54
    }, {
        name: 'Domingo / 6 p.m / La Libertad / Smart Phone / DoubleClick  / 300x600',
        amount: 450,
        percentage: 33
    }, {
        name: 'Lunes / 12 a.m / La Libertad / Smart Phone / DoubleClick  / 300x250',
        amount: 450,
        percentage: 32
    }, {
        name: 'Martes / 10 a.m / Lima / Smart Phone / DoubleClick  / 300x600',
        amount: 260,
        percentage: 21
    }, {
        name: 'Domingo / 10 a.m / Lima / Smart Phone / DoubleClick  / 300x600',
        amount: 230,
        percentage: 11
    }, {
        name: 'Domingo / 3 p.m / Barranca / Smart Phone / DoubleClick  / 300x600',
        amount: 230,
        percentage: 22
    }, {
        name: 'Martes / 3 p.m / Lima / Smart Phone / DoubleClick  / 300x600',
        amount: 230,
        percentage: 33
    }, {
        name: 'Miercoles / 12 p.m / Lima / Smart Phone / DoubleClick  / 300x600',
        amount: 230,
        percentage: 44
    }, {
        name: 'Domingo / 1 p.m / La Libertad / Smart Phone / DoubleClick  / 300x600',
        amount: 200,
        percentage: 55
    }, {
        name: 'Domingo / 11 p.m / Lima / Smart Phone / DoubleClick  / 300x600',
        amount: 200,
        percentage: 66
    }];

    var settings = {
        // width: parseInt(d3.select('body').style('width'), 10),
        // height: parseInt(d3.select('body').style('height'), 10),
        width: 900,
        height: 700,
        axisMargin: 10,
        margin: 40,
        barHeight: 0,
        barPadding: 0,
        scale: 0,
        max: d3.max(data, function(d) { return d.amount; }),
        labelWidth: 0,
        percentageWidth: 0,
        amountWidth: 0,
        labelsPadding: 25,
        captionsWidth: function() {
            return this.labelWidth + this.labelsPadding + this.percentageWidth + this.labelsPadding + this.amountWidth;
        }
    };

    settings.barHeight = (settings.height - settings.axisMargin - settings.margin * 2) * 0.4 / data.length;
    settings.barPadding = (settings.height - settings.axisMargin - settings.margin) * 0.6 / data.length;

    var chart = d3.select('body')
        .append('svg')
        .attr('width', settings.width)
        .attr('height', settings.height);

    var bar = chart.selectAll('g')
        .data(data)
        .enter()
        .append('g');

    bar.attr('class', 'bar')
        .attr('cx', 0)
        .attr('transform', function(d, i) {
            return 'translate(' + settings.margin + ', ' + (i * (settings.barHeight + settings.barPadding) + settings.barPadding) + ')';
        });

    bar.append('text')
        .attr('class', 'label')
        .attr('x', 0)
        // .attr('y', 0)
        .text(function(d) { return d.name; })
        .each(function() {
            settings.labelWidth = Math.ceil(Math.max(settings.labelWidth, this.getBBox().width));
        });

    bar.append('text')
        .attr('class', 'percentage')
        .attr('x', settings.labelWidth + settings.percentageWidth + settings.labelsPadding)
        // .attr('y', 100)
        .text(function (d) {})//aqui va porcentaje//
        .each(function() {
            settings.percentageWidth = Math.ceil(Math.max(settings.percentageWidth, this.getBBox().width));
        });

    bar.append('text')
        .attr('class', 'amount')
        .attr('x', settings.labelWidth + settings.percentageWidth + settings.amountWidth + settings.labelsPadding * 2)
        // .attr('y', 200)
        .text(function(d) { return d.amount; }).each(function() {
            settings.amountWidth = Math.ceil(Math.max(settings.amountWidth, this.getBBox().width));
        });

    settings.scale = d3.scaleLinear()
        .domain([0, settings.max])
        .range([0, settings.width - settings.margin * 2 - (settings.labelWidth + settings.percentageWidth + settings.amountWidth + settings.labelsPadding * 2)]);

    bar.append('rect')
        .attr('transform', 'translate(' + settings.captionsWidth() + ', 0)')
        .attr('height', settings.barHeight)
        .attr('width', function(d) {
            return settings.scale(d.amount);
        })
        .attr('x', settings.labelsPadding)
        .attr('y', -settings.barHeight)
        .attr('fill', '#FF7043');

    bar.append('rect')
        .attr('transform', 'translate(' + settings.captionsWidth() + ', 0)')
        .attr('height', settings.barHeight)
        .attr('width', function(d) {
            return settings.scale(d.amount * (d.percentage / 100));
        })
        .attr('x', settings.labelsPadding)
        .attr('y', -settings.barHeight)
        .attr('fill', '#FF7043');

    console.log('data length: ', data.length);
    console.log('data: ', data);
    console.log('settings: ', settings);

})(window.d3);